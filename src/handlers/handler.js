import axios from 'axios'
import config from '../config'

export const getRoute = async (startPoint, endPoint) => {
  const response = await axios.post(`${config.backendUrl}/path_finders/find`, {
    path_finder: {
      start: [1,1],
      destination: [7,7]
    }
  });
  return response;
}
