import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import axios from 'axios'
import config from './config'
import MenuComponent from "./components/menu/Menu"
import MapComponent from './components/map/Map'
import SnackBarPopup from './components/snackBarPopup/SnackBarPopup'
import './App.scss';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      snackBarPopupValue: false,
      routePath: {}
    }
  }

  onUpdateSnackBarPopup = (value) => {
    this.setState({
      snackBarPopupValue: value
    })
    console.log('PARENT STATE', this.state)
  }

  getRoutePath = async (startx, starty, endx, endy, vehicleHeight, vehicleWidth, vehicleWeight) => {
    const response = await axios.post(`${config.backendUrl}/path_finders/find`, {
      path_finder: {
        start: [startx, starty],
        destination: [endx, endy]
      },
      vehicle: {
        height: vehicleHeight,
        width: vehicleWidth,
        weight: vehicleWeight
      }
    });
    this.setState({routePath: response.data})
    this.onUpdateSnackBarPopup(true)
  }

  render() {
    return (
      <Grid container className="App">
        <Grid item xs={12} md={2} className="Menu">
          <MenuComponent parentState={this.state} onUpdateSnackBarPopup={this.onUpdateSnackBarPopup} getRoutePath={this.getRoutePath}/>
        </Grid>
        <Grid item xs={12} md={10}>
          <MapComponent routePath={this.state.routePath}/>
          <SnackBarPopup snackBarPopupValue={this.state.snackBarPopupValue} onUpdateSnackBarPopup={this.onUpdateSnackBarPopup}/>
        </Grid>
      </Grid>
    );
  }
}

export default App;
