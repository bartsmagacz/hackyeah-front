import React from 'react';
import './Map.scss';

class MenuComponent extends React.Component {

  createChild = (response, x, y) => {
    let road;
    const classNames = ((response.length > 0 && (response.filter((ele) => ele.x_axis === x && ele.y_axis === y)).length === 1)) ? 'mapElement active' : 'mapElement';
    if ((x !== 1 && x !== 10) && (y !== 1 && y !== 10)) {
        road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="road22a">

        </span>
        <span className="road22b">

        </span>
      </span>)
    } else if (x === 1 && y === 1) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx1ay1">

        </span>
        <span className="roadx1by1">

        </span>
      </span>)
    } else if (x === 1 && (y !== 1 && y !== 10)) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx1ayr">

        </span>
        <span className="roadx1byr">

        </span>
      </span>)
    } else if (x === 1 && y === 10) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx1ay10">

        </span>
        <span className="roadx1by10">

        </span>
      </span>)
    } else if ((x !== 1 && x !== 10) && y === 1) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadxray1">

        </span>
        <span className="roadxrby1">

        </span>
      </span>)
    } else if (x === 10 && y === 1) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx10ay1">

        </span>
        <span className="roadx10by1">

        </span>
      </span>)
    } else if (x === 10 && (y !== 1 && y !== 10)) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx10ayr">

        </span>
        <span className="roadx10byr">

        </span>
      </span>)
    } else if (x === 10 && y === 10) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadx10ay10">

        </span>
        <span className="roadx10by10">

        </span>
      </span>)
    } else if ((x !== 1 && x !== 10) && y === 10) {
      road = (<span key={`road-${x}-${y}`} id={`road-${x}-${y}`} className='road22'>
        <span className="roadxray10">

        </span>
        <span className="roadxrby10">

        </span>
      </span>)
    }

    return <div className={classNames} xaxis={x} yaxis={y} id={`${x}-${y}`} key={`${x}-${y}`}>
            <span key={`coords-${x}-${y}`} className="coordsElement">
              [{x}-{y}]
            </span>
            {road}
    </div>
  }

  createMap = (response) => {
    let map = [];
    for (let y = 1; y < 11; y++) {
      let children = []
          for (let x = 1; x < 11; x++) {
            children.push(this.createChild(response, x, y))
          }
      map.push(<div className="mapRow" id={y} key={y}>{children}</div>)
    }
    return map;
  }

  render() {
    const createMapArr = this.createMap(this.props.routePath)
    //console.log('response', (response.filter((ele) => ele.x_axis === 3)).length)
    console.log('createMapArr', createMapArr[4].props.children[4]);
    return (
      <div className="MapComponent">
          {createMapArr}
      </div>
    );
  }
}

export default MenuComponent;
