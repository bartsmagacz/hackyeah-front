import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import './Menu.scss'

class MenuComponent extends React.Component {

  handleClick = async () => {
    console.log('this.state child menu', this.state)
    this.props.getRoutePath(
      this.state.startx,
      this.state.starty,
      this.state.endx,
      this.state.endy,
      this.state.vehicleHeight,
      this.state.vehicleWidth,
      this.state.vehicleWeight)
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    return (
      <div>
        <div className="coordsInputs">
          <TextField
            id="startx"
            label="StartX"
            onChange={this.handleChange('startx')}
            margin="normal"
            className="coordsInput"
          />
          <TextField
            id="starty"
            label="StartY"
            onChange={this.handleChange('starty')}
            margin="normal"
            className="coordsInput"
          />
        </div>
        <div className="coordsInputs">
          <TextField
            id="endx"
            label="EndX"
            onChange={this.handleChange('endx')}
            margin="normal"
            className="coordsInput"
          />
          <TextField
            id="endy"
            label="EndY"
            onChange={this.handleChange('endy')}
            margin="normal"
            className="coordsInput"
          />
        </div>
        <TextField
          id="vehicleHeight"
          label="Vehicle Height"
          onChange={this.handleChange('vehicleHeight')}
          margin="normal"
        />
        <TextField
          id="vehicleWidth"
          label="Vehicle Width"
          onChange={this.handleChange('vehicleWidth')}
          margin="normal"
        />
        <TextField
          id="vehicleWeight"
          label="Vehicle Weight"
          onChange={this.handleChange('vehicleWeight')}
          margin="normal"
        />
        <Button id="requestButton"
          onClick={this.handleClick}
          className="requestButton"
        >
          Send request
        </Button>
      </div>
    );
  }
}

export default MenuComponent;
